﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using LibPollutionformules;
using System;
namespace TestProject1
{
    [TestClass]
    public class AirPollTests
    {
        AirPollutionCountClass f1 = new AirPollutionCountClass();
        double H = 80;
        double D = 6.4;
        double V = 1198800;
        int Tr = 100;
        int Ta = 30;
        double Zo = 0.1;
        double nu = 75;

        double A = 160;
        [TestMethod]
        public void TestWo_AverageVelocityOutfall()
        {
            double result = f1.Wo_AverageVelocityOutfall(V, D);
            Assert.AreEqual(10.3565, result);
        }
        [TestMethod]
        public void TestV1_MixtureVolume()
        {
            double result = f1.V1_MixtureVolume(V, D);
            Assert.AreEqual(333, result);
        }
        [TestMethod]
        public void TestM_DustAmount()
        {
            double result = f1.M_DustAmount(V, D, Zo);
            Assert.AreEqual(33.3, result);
        }
        [TestMethod]
        public void TestFCoeff()
        {
            f1.Velocity = 1;
            f1.E = 0;
            double result = f1.f_Coefficient(V, D, H, Tr, Ta);
            Assert.AreEqual(1.53, Math.Round(result, 2));
        }
        [TestMethod]
        public void TestMCoeff()
        {

            AirPollutionCountClass f2 = new AirPollutionCountClass();
            f2.Velocity = 1;
            f2.E = 0;
            double result = f2.m_Coefficient(V, D, H, Tr, Ta);
            Assert.AreEqual(0.82, result);
        }

        [TestMethod]
        public void TestVm_Coefficient()
        {
            f1.Velocity = 1;
            f1.E = 0;
            double result = f1.Vm_Coefficient(V, D, H, Tr, Ta);
            Assert.AreEqual(4.31, result);
        }
        [TestMethod]
        public void Testn_Coefficient()
        {
            f1.Velocity = 1;
            f1.E = 0;
            double result = f1.n_Coefficient(V, D, H, Tr, Ta);
            Assert.AreEqual(1, result);
        }
        [TestMethod]
        public void TestF_Coefficient()
        {
            f1.Velocity = 1;
            f1.E = 0;
            double result = f1.F_Coefficient(f1.E, nu);
        }
        [TestMethod]
        public void TestCm_MaxPollutionConcentration()
        {
            f1.Velocity = 1;
            f1.E = 0;
            double result = f1.Cm_MaxPollutionConcentration(A, V, D, Zo, f1.E, nu, Tr, Ta, H);
            Assert.AreEqual(0.06, result);
        }
        [TestMethod]
        public void Testd_Coefficient()
        {
            f1.Velocity = 1;
            f1.E = 0;
            double result = f1.d_Coefficient(V, D, H, Tr, Ta, Zo);
            Assert.AreEqual(19.22, result);
        }
        [TestMethod]
        public void TestXm_TorchDistance()
        {
            f1.Velocity = 1;
            f1.E = 0;
            double result = f1.Xm_TorchDistance(f1.E, nu, V, D, H, Tr, Ta, Zo);
            Assert.AreEqual(961, result);
        }
        [TestMethod]
        public void Testum_DangerousWindVelocity()
        {
            f1.Velocity = 1;
            f1.E = 0;
            double result = f1.um_DangerousWindVelocity(V, D, H, Tr, Ta);
            Assert.AreEqual(4.95, result);
        }
        [TestMethod]
        public void Testr_CoefficientCount()
        {
            f1.Velocity = 1;
            f1.E = 0;
            double result = f1.r_CoefficientCount(V, D, H, Tr, Ta)[3];
            Assert.AreEqual(0.976, result);

        }
        [TestMethod]
        public void TestCmv_MaxPolutionConcentrationVelocityCount()
        {
            f1.Velocity = 1;
            f1.E = 0;
            double result = f1.Cmv_MaxPolutionConcentrationVelocityCount(V, D, H, Tr, Ta, Zo, A, f1.E, nu)[3];
            Assert.AreEqual(0.0586, result);
        }
        [TestMethod]
        public void TestXmu_MaxConcentrationDistanceCount()
        {
            f1.Velocity = 1;
            f1.E = 0;
            double result = f1.Xmu_MaxConcentrationDistanceCount(f1.E, nu, V, D, H, Tr, Ta, Zo)[3];
            Assert.AreEqual(1026, result);
        }
        [TestMethod]
        public void TestS1_CoefficientCount()
        {
            f1.Velocity = 1;
            f1.E = 0;
            double result = f1.S1_CoefficientCount(f1.E, nu, V, D, H, Tr, Ta, Zo)[4];
            Assert.AreEqual(0.022, Math.Round(result, 3));
        }
        [TestMethod]
        public void TestCx_ConcentrationCountOfDistanceCount()
        {
            f1.Velocity = 1;
            f1.E = 0;
            double result = f1.Cx_ConcentrationCountOfDistanceCount(f1.E, nu, V, D, H, Tr, Ta, Zo, A)[4];
            Assert.AreEqual(0.0013, Math.Round(result, 4));
        }
        [TestMethod]
        public void TestS2_CoefficientCount()
        {
            f1.Velocity = 1;
            f1.E = 0;
            double result = f1.S2_CoefficientCount(f1.Velocity)[4, 4];
            Assert.AreEqual(0.99, Math.Round(result, 2));
        }
        [TestMethod]
        public void TestCy_ConcentrationCountOfDistanceCount()
        {
            f1.Velocity = 1;
            f1.E = 0;
            double result = f1.Cy_ConcentrationCountOfDistanceCount(f1.E, nu, V, D, H, Tr, Ta, Zo, A)[4, 4];
            Assert.AreEqual(0.0013, Math.Round(result, 4));
        }
        [TestMethod]
        public void TestConcentrationCountOfDistanceCount()
        {
            int k = 0, j = 0;
            double E = f1.E;
            var result = Math.Round(f1.Cy_ConcentrationCountOfDistanceCount(Convert.ToDouble(E), Convert.ToDouble(nu), Convert.ToDouble(V), Convert.ToDouble(D), Convert.ToDouble(H), Convert.ToInt32(Tr), Convert.ToInt32(Ta), Convert.ToDouble(Zo), Convert.ToInt32(A))[k, j], 4);

            Assert.AreEqual(0.0593, result);
        }



        [TestMethod]
        public void TestDangerVelocity()
        {

            var result = f1.um_DangerousWindVelocity(Convert.ToDouble(V), Convert.ToDouble(D), Convert.ToDouble(H), Convert.ToInt32(Tr), Convert.ToInt32(Ta));
            Assert.AreEqual(4.95, result);
        }
    }
}
