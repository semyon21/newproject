﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prilogeney.Reports
{
    public class MyParameter
    {
        public string Velocity{ get; set; }

        public string Dust { get; set; }
        public string Distance { get; set; }

        public MyParameter(string speed, string pal, string dist)
        {
            Velocity = speed;
            Dust = pal;
            Distance = dist;
        }
        public MyParameter()
        {
        }
    }
}
